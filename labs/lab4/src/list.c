#include <list.h>

static const int INITIAL_CAPASITY = 16;

struct __NumList{
    void ** item;
    int capasity;
    int length;
};

static void incMemory(List * self){
    self->capasity *= 2;
    self->item = realloc(self->item, self->capasity * sizeof(int));
    
    if(self->item == NULL){
        fprintf(stderr, "Out of memory item\n");
        assert(0);
        abort();
    }
}

static void checkIndex(List * self, int index){
    if(index > self->length){
        assert(0 && "Invalid index");
        fprintf(stderr, "Invalid index\n");
        abort();
    }
}

List * List_new(void) {
    List * self = malloc(sizeof(List));
    if(self == NULL) {
        fprintf(stderr, "Out of memory self\n");
        assert(0);
        abort();
    }
    self->capasity = INITIAL_CAPASITY;
    self->length = 0;
    self->item = malloc(self->capasity * sizeof(int));
    
    if(self->item == NULL) {
        fprintf(stderr, "Out of memory item\n");
        assert(0);
        abort();
    }

    return self;
}
void List_free(List * self) {
    free(self->item);
    free(self);
}

void List_insert(List * self, void * value, size_t index) {
    checkIndex(self, index);
    if(self->length == self->capasity){
        incMemory(self);
    }
    int count = List_count(self);
    if(count > 0)
        for(int i = count; i > index; i--){
            self->item[i] = self->item[i - 1];
        }
    self->item[index] = value;
    self->length++;
}
void List_add(List * self, void * value) {
    if(self->length == self->capasity){
        incMemory(self);
    }
    self->item[self->length] = value;
    self->length++;
}
void * List_at(List * self, size_t index) {
    checkIndex(self, index);
    return self->item[index];
}
void * List_set(List * self, size_t index, void * value) {
    checkIndex(self, index);
    void * temp = self->item[index];
    self->item[index] = value;
    return temp;
}
void * List_removeAt(List * self, size_t index) {
    checkIndex(self, index);
    void * temp = self->item[index];
    for(int i = index + 1; i < self->length; i++){
        self->item[i - 1] = self->item[i];
    }
    self->length--;
    
    if(self->length <= self->capasity / 3 && self->capasity > INITIAL_CAPASITY){
        self->capasity /= 2;
        self->item = realloc(self->item, self->capasity * sizeof(int));
    }

    return temp;
}
size_t List_count(List * self) {
    return self->length;
}