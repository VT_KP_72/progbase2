#include <string_buffer.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
 
const int INITIAL_CAPACITY = 256;
 
static char * strDup(const char *s) {
    char *d = malloc(strlen(s) + 1);   // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy(d, s);                        // Copy the characters
    return d;                            // Return the new string
}
 
 
typedef struct __StringBuffer {
    char * buffer;
    size_t capacity;
    size_t length;
 
} StringBuffer;
 
StringBuffer * StringBuffer_new(void){
    StringBuffer * self = malloc(sizeof(StringBuffer));
    self->capacity = INITIAL_CAPACITY;
    self->buffer = malloc(sizeof(char) * self->capacity);
    self->buffer[0] = '\0';
    self->length = 1;
    return self;
}
void StringBuffer_free(StringBuffer * self){
    free(self->buffer);
    free(self);
}
 
static void ensureCapacity(StringBuffer * self, int appendLength){
     if(self->length + appendLength >= self->capacity){
        size_t newCapacity = self->capacity * 2;
        char * newBuffer = realloc(self->buffer, sizeof(char) * newCapacity);
        if(newBuffer == NULL){
            // @todo exeption
        }
        else {
            self->buffer = newBuffer;
            self->capacity = newCapacity;
        }
    }
}
 
void StringBuffer_append(StringBuffer * self, const char * str){
    size_t len = strlen(str);
   
    ensureCapacity(self, len);
 
    strcat(self->buffer + (self->length - 1), str);
    self->length += len;
}
void StringBuffer_appendChar(StringBuffer * self, char ch){
    ensureCapacity(self, 1);
 
    self->buffer[self->length] = '\0';
    self->buffer[self->length - 1] = ch;
    self->length++;
}
void StringBuffer_appendFormat(StringBuffer * self, const char * fmt){
    //@todo
    assert(0 && "Not implecated");
}
 
void StringBuffer_clear(StringBuffer * self){
    self->buffer[0] = '\0';
    self->length = 1;
}
char * StringBuffer_toNewString(StringBuffer * self){
    //char * temp = strDup(self->buffer);
 
   // char * name = temp;
    //strcpy(name, temp);
   // free(temp);
    //return name;
 
    return strDup(self->buffer);
}
// #include <string_buffer.h>
// #include <string.h>
// #include <assert.h>

// const int INITIAL_CAPACITY = 16;

// static char * strDup(const char *s) {
//     char *d = malloc(strlen(s) + 1);   // Space for length plus nul
//     if (d == NULL) return NULL;          // No memory
//     strcpy(d, s);                        // Copy the characters
//     return d;                            // Return the new string
// }


// typedef struct __StringBuffer {
//     char * buffer;
//     size_t capacity;
//     size_t length;

// } StringBuffer;

// StringBuffer * StringBuffer_new(void){
//     StringBuffer * self = malloc(sizeof(StringBuffer));
//     self->capacity = INITIAL_CAPACITY;
//     self->buffer = malloc(sizeof(char) * self->capacity);
//     self->buffer[0] = '\0';
//     self->length = 1;
//     return self; 
// }
// void StringBuffer_free(StringBuffer * self){
//     free(self->buffer);
//     free(self);
// }

// static void ensureCapacity(StringBuffer * self, int appendLength){
//      if(self->length + appendLength > self->capacity){
//         size_t newCapacity = self->capacity * 2;
//         char * newBuffer = realloc(self->buffer, newCapacity * sizeof(char));
//         if(newBuffer == NULL){
//             // @todo exeption
//         }
//         else {
//             self->buffer = newBuffer;
//             self->capacity = newCapacity;
//         }
//     }
// }

// void StringBuffer_append(StringBuffer * self, const char * str){
//     size_t len = strlen(str);
    
//     ensureCapacity(self, len);

//     strcat(self->buffer + (self->length - 1), str);
//     self->length += len;
// }
// void StringBuffer_appendChar(StringBuffer * self, char ch){
//     self->buffer[self->length] = '\0';
//     self->buffer[self->length - 1] = ch;
//     self->length++;
// }
// void StringBuffer_appendFormat(StringBuffer * self, const char * fmt){
//     assert(0 && "Not implecated");
// }

// void StringBuffer_clear(StringBuffer * self){
//     self->buffer[0] = '\0';
//     self->length = 1;
// }
// char * StringBuffer_toNewString(StringBuffer * self){
//     return strDup(self->buffer);
// }