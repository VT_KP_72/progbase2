
#include <list.h>
typedef struct __CsvTable CsvTable;
typedef struct __CsvRow   CsvRow;



CsvRow * CsvRow_new(void);
void CsvRow_free(CsvRow * self);
void CsvRow_add(CsvRow * self, const char * value);
void CsvRow_values(CsvRow * self, List * values);
void CsvRow_remuve(CsvRow * self,int i);

CsvTable * CsvTable_new(void);
void CsvTable_free(CsvTable * self);
void CsvTable_add (CsvTable * self, CsvRow * row);
void CsvTable_rows(CsvTable * self, List * rows);
void CsvTable_remuve(CsvTable * self,int i);

CsvTable * CsvTable_newFromString(const char * csvString);
char *     CsvTable_toNewString  (CsvTable * self);