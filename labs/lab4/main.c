#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include <list.h>
#include <csv.h>

#define N 10
int S = 0;

#define foreach(VARTYPE, VARNAME, LIST)   \
   for (int VARNAME##_i = 0, VARNAME##_length = List_count(LIST); !VARNAME##_i; ++VARNAME##_i) \
       for (VARTYPE VARNAME = (VARTYPE)List_at(LIST, VARNAME##_i); \
           VARNAME##_i < VARNAME##_length; \
           VARNAME = (VARTYPE)List_at(LIST, ++VARNAME##_i))
	


void printCsvTable(CsvTable * self){
    List * rows = List_new();
    CsvTable_rows(self, rows);
    foreach(CsvRow *, row, rows){
        List * values = List_new();
        CsvRow_values(row, values);
        foreach(char *, value, values){
            printf("%s", value);
        }
        puts("");
        List_free(values);
    }
    List_free(rows);
}

// typedef struct __City City;
// struct __City {
//     char namber;
//     char name;
//     char size;
//     int length;
// };
// int ArrSize(City ** citys) {
//     int len = 0;
//     while (citys[len] != NULL) {
//         ++len;
//     }
//     return len;
// }

// City * City_new(char namber, char name, char size);
CsvTable * addFRow(CsvTable * t);
int main(void) {
    int size = 0;
    puts("====================");
    CsvTable * t = CsvTable_new();
    //City * citys = City_new(namber, name, size);

    // addFRow(t);
    // printf("%d \n", S);
    for (int i = 0; i < 2; i++) {
        addFRow(t);
        printf("%d \n", S);

    }
    // CsvTable_add(t, self);
    // printCsvTable(t);
    //printf("%d \n", S);
    CsvTable_remuve(t,S - 1);
    char * csvStr = CsvTable_toNewString(t);
    
    char * csvString = CsvTable_toNewString(t);
    puts("");
    puts(csvStr);
    puts("====================");
    CsvTable * newTable = CsvTable_newFromString(csvString);
    free(csvString);
    printCsvTable(newTable);
    
    return 0;
}

// City * City_new(char namber, char name, char size) {
//     City * self = malloc(sizeof(City));
//     self->length = self->length + 1;
//     self->namber = namber;
//     self->name = name;
//     self->size = size;
//     return self;
// }
CsvTable * addFRow(CsvTable * t) {
    if (S < N) {
        List * list = List_new(); 
        char namber[20];
        char name[20];
        char size[20];
        printf("Input City namber: ");
        scanf("%s", namber);
    
        printf("Input City name: ");
        scanf("%s", name);
    
        printf("Input City size: ");
        scanf("%s", size);
    
        CsvRow * self = CsvRow_new();
        S++;   
        char* c;
        c = (char *)malloc(10 * sizeof(char)); 
        int v = 0;
        while (S > 9)
        {
            c[v++] = (S % 10) + '0';
            S = S / 10;
        }
        c[v++] = S + '0';
        c[v] = '\0'; 
        char q;
        for (int i = 0; i < v / 2; i++)
        {
            q = c[i];
            c[i] = c[v - 1 - i];
            c[v - 1 - i] = q;
        }
        v = 0;
        free(c);
        //CsvRow_add(self, c);
        CsvRow_add(self, namber);
        CsvRow_add(self, name);
        CsvRow_add(self, size);
        CsvTable_add(t, self);
    } else {
        printf("MAX SIZE ERROR\n");
    }
    return t;
}