/********************************************************************************
** Form generated from reading UI file 'dialogedit.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGEDIT_H
#define UI_DIALOGEDIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DialogEdit
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_CityNameEdit;
    QLabel *label_PresedentNameEdit;
    QLabel *label_averageEdit;
    QVBoxLayout *verticalLayout;
    QLineEdit *NameCityEdit;
    QLineEdit *PresedentNameEdit;
    QLineEdit *averageEdit;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *DialogEdit)
    {
        if (DialogEdit->objectName().isEmpty())
            DialogEdit->setObjectName(QStringLiteral("DialogEdit"));
        DialogEdit->resize(239, 150);
        gridLayout = new QGridLayout(DialogEdit);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_CityNameEdit = new QLabel(DialogEdit);
        label_CityNameEdit->setObjectName(QStringLiteral("label_CityNameEdit"));

        verticalLayout_2->addWidget(label_CityNameEdit);

        label_PresedentNameEdit = new QLabel(DialogEdit);
        label_PresedentNameEdit->setObjectName(QStringLiteral("label_PresedentNameEdit"));

        verticalLayout_2->addWidget(label_PresedentNameEdit);

        label_averageEdit = new QLabel(DialogEdit);
        label_averageEdit->setObjectName(QStringLiteral("label_averageEdit"));

        verticalLayout_2->addWidget(label_averageEdit);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        NameCityEdit = new QLineEdit(DialogEdit);
        NameCityEdit->setObjectName(QStringLiteral("NameCityEdit"));

        verticalLayout->addWidget(NameCityEdit);

        PresedentNameEdit = new QLineEdit(DialogEdit);
        PresedentNameEdit->setObjectName(QStringLiteral("PresedentNameEdit"));

        verticalLayout->addWidget(PresedentNameEdit);

        averageEdit = new QLineEdit(DialogEdit);
        averageEdit->setObjectName(QStringLiteral("averageEdit"));

        verticalLayout->addWidget(averageEdit);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);

        buttonBox = new QDialogButtonBox(DialogEdit);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout_3->addWidget(buttonBox);


        gridLayout->addLayout(verticalLayout_3, 0, 0, 1, 1);


        retranslateUi(DialogEdit);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogEdit, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogEdit, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogEdit);
    } // setupUi

    void retranslateUi(QDialog *DialogEdit)
    {
        DialogEdit->setWindowTitle(QApplication::translate("DialogEdit", "Edit City", 0));
        label_CityNameEdit->setText(QApplication::translate("DialogEdit", "City Name", 0));
        label_PresedentNameEdit->setText(QApplication::translate("DialogEdit", "Presedent Name", 0));
        label_averageEdit->setText(QApplication::translate("DialogEdit", "average", 0));
    } // retranslateUi

};

namespace Ui {
    class DialogEdit: public Ui_DialogEdit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGEDIT_H
