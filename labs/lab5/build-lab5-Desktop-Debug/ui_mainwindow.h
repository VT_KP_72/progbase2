/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSave;
    QAction *actionDownload;
    QAction *actionOpen;
    QAction *actionSave_2;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton_Add;
    QPushButton *pushButton_Remove;
    QPushButton *pushButton_Edit;
    QLineEdit *lineEdit_Execute;
    QPushButton *pushButtonExecute;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_City;
    QLabel *label_Result;
    QHBoxLayout *horizontalLayout;
    QListWidget *listWidget;
    QListWidget *listWidget_result;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_CityName;
    QLabel *label_PresidentName;
    QLabel *label_CityAverage;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_City_Name;
    QLabel *label_Precident_Name;
    QLabel *label_City_Average;
    QHBoxLayout *horizontalLayout_4;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(641, 503);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave->setCheckable(true);
        actionSave->setChecked(true);
        actionDownload = new QAction(MainWindow);
        actionDownload->setObjectName(QStringLiteral("actionDownload"));
        actionDownload->setCheckable(true);
        actionDownload->setChecked(true);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave_2 = new QAction(MainWindow);
        actionSave_2->setObjectName(QStringLiteral("actionSave_2"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        pushButton_Add = new QPushButton(centralWidget);
        pushButton_Add->setObjectName(QStringLiteral("pushButton_Add"));

        horizontalLayout_3->addWidget(pushButton_Add);

        pushButton_Remove = new QPushButton(centralWidget);
        pushButton_Remove->setObjectName(QStringLiteral("pushButton_Remove"));

        horizontalLayout_3->addWidget(pushButton_Remove);

        pushButton_Edit = new QPushButton(centralWidget);
        pushButton_Edit->setObjectName(QStringLiteral("pushButton_Edit"));

        horizontalLayout_3->addWidget(pushButton_Edit);

        lineEdit_Execute = new QLineEdit(centralWidget);
        lineEdit_Execute->setObjectName(QStringLiteral("lineEdit_Execute"));

        horizontalLayout_3->addWidget(lineEdit_Execute);

        pushButtonExecute = new QPushButton(centralWidget);
        pushButtonExecute->setObjectName(QStringLiteral("pushButtonExecute"));

        horizontalLayout_3->addWidget(pushButtonExecute);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_City = new QLabel(centralWidget);
        label_City->setObjectName(QStringLiteral("label_City"));

        horizontalLayout_2->addWidget(label_City);

        label_Result = new QLabel(centralWidget);
        label_Result->setObjectName(QStringLiteral("label_Result"));

        horizontalLayout_2->addWidget(label_Result);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        horizontalLayout->addWidget(listWidget);

        listWidget_result = new QListWidget(centralWidget);
        listWidget_result->setObjectName(QStringLiteral("listWidget_result"));

        horizontalLayout->addWidget(listWidget_result);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_CityName = new QLabel(centralWidget);
        label_CityName->setObjectName(QStringLiteral("label_CityName"));

        horizontalLayout_6->addWidget(label_CityName);

        label_PresidentName = new QLabel(centralWidget);
        label_PresidentName->setObjectName(QStringLiteral("label_PresidentName"));

        horizontalLayout_6->addWidget(label_PresidentName);

        label_CityAverage = new QLabel(centralWidget);
        label_CityAverage->setObjectName(QStringLiteral("label_CityAverage"));

        horizontalLayout_6->addWidget(label_CityAverage);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_City_Name = new QLabel(centralWidget);
        label_City_Name->setObjectName(QStringLiteral("label_City_Name"));

        horizontalLayout_7->addWidget(label_City_Name);

        label_Precident_Name = new QLabel(centralWidget);
        label_Precident_Name->setObjectName(QStringLiteral("label_Precident_Name"));

        horizontalLayout_7->addWidget(label_Precident_Name);

        label_City_Average = new QLabel(centralWidget);
        label_City_Average->setObjectName(QStringLiteral("label_City_Average"));

        horizontalLayout_7->addWidget(label_City_Average);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));

        verticalLayout->addLayout(horizontalLayout_4);


        gridLayout->addLayout(verticalLayout, 1, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 641, 27));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave_2);
        mainToolBar->addAction(actionSave);
        mainToolBar->addAction(actionDownload);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "lab5_title", 0));
        actionSave->setText(QApplication::translate("MainWindow", "Save", 0));
        actionDownload->setText(QApplication::translate("MainWindow", "Open", 0));
        actionOpen->setText(QApplication::translate("MainWindow", "Open..", 0));
        actionSave_2->setText(QApplication::translate("MainWindow", "Save..", 0));
        pushButton_Add->setText(QApplication::translate("MainWindow", "Add", 0));
        pushButton_Remove->setText(QApplication::translate("MainWindow", "Remove", 0));
        pushButton_Edit->setText(QApplication::translate("MainWindow", "Edit", 0));
        pushButtonExecute->setText(QApplication::translate("MainWindow", "Execute", 0));
        label_City->setText(QApplication::translate("MainWindow", "City", 0));
        label_Result->setText(QApplication::translate("MainWindow", "Result", 0));
        label_CityName->setText(QApplication::translate("MainWindow", "City name", 0));
        label_PresidentName->setText(QApplication::translate("MainWindow", "President name", 0));
        label_CityAverage->setText(QApplication::translate("MainWindow", "City area", 0));
        label_City_Name->setText(QString());
        label_Precident_Name->setText(QString());
        label_City_Average->setText(QString());
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
