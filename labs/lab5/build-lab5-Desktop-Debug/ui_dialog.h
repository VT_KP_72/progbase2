/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label_CityName;
    QLabel *label_PresedentName;
    QLabel *label_average;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *NameCity;
    QLineEdit *PresedentName;
    QLineEdit *average;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QStringLiteral("Dialog"));
        gridLayout = new QGridLayout(Dialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_CityName = new QLabel(Dialog);
        label_CityName->setObjectName(QStringLiteral("label_CityName"));

        verticalLayout->addWidget(label_CityName);

        label_PresedentName = new QLabel(Dialog);
        label_PresedentName->setObjectName(QStringLiteral("label_PresedentName"));

        verticalLayout->addWidget(label_PresedentName);

        label_average = new QLabel(Dialog);
        label_average->setObjectName(QStringLiteral("label_average"));

        verticalLayout->addWidget(label_average);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        NameCity = new QLineEdit(Dialog);
        NameCity->setObjectName(QStringLiteral("NameCity"));

        verticalLayout_2->addWidget(NameCity);

        PresedentName = new QLineEdit(Dialog);
        PresedentName->setObjectName(QStringLiteral("PresedentName"));

        verticalLayout_2->addWidget(PresedentName);

        average = new QLineEdit(Dialog);
        average->setObjectName(QStringLiteral("average"));

        verticalLayout_2->addWidget(average);


        horizontalLayout->addLayout(verticalLayout_2);


        verticalLayout_3->addLayout(horizontalLayout);

        buttonBox = new QDialogButtonBox(Dialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout_3->addWidget(buttonBox);


        gridLayout->addLayout(verticalLayout_3, 0, 0, 1, 1);


        retranslateUi(Dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Add City", 0));
        label_CityName->setText(QApplication::translate("Dialog", "City Name", 0));
        label_PresedentName->setText(QApplication::translate("Dialog", "Presedent Name", 0));
        label_average->setText(QApplication::translate("Dialog", "average", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
