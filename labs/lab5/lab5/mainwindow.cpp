#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "city.h"
#include "dialog.h"
#include "dialogedit.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QFileDialog>
#include <QJsonArray>
#include <QMessageBox>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_Add_clicked()
{
    Dialog *dialog = new Dialog();
    QLineEdit *addNameCity = dialog->findChild<QLineEdit*>("NameCity");
    QLineEdit *addPresedentName = dialog->findChild<QLineEdit*>("PresedentName");
    QLineEdit *addAverage = dialog->findChild<QLineEdit*>("average");
    dialog->show();
    int result = dialog->exec();
    if (result == 1)
    {
        if (addNameCity->text().toStdString()!="")
        {
            QString CityName = addNameCity->text();
            QString presidentName = addPresedentName->text();
            int average=addAverage->text().toInt();
            if (average < 1)
            {
                QMessageBox::critical(this, tr("EROOR"),tr("Enter an integer in a string 'average'"));
                return;
            }
            City city(CityName, presidentName, average);

            QVariant qVariant;
            qVariant.setValue(city);
            QListWidgetItem *qCityListItem = new QListWidgetItem();
            qCityListItem->setText(city.getCityName());
            qCityListItem->setData(Qt::UserRole, qVariant);

            ui->listWidget->addItem(qCityListItem);
        }
    }
}

void MainWindow::on_pushButton_Remove_clicked()
{
    if (ui->listWidget->currentRow() == -1)
    {
        QMessageBox::critical(this, tr("EROOR"),tr("Item not found"));
        return;
    }
    delete ui->listWidget->currentItem();
}

void MainWindow::on_pushButton_Edit_clicked()
{
    if (ui->listWidget->currentRow() == -1)
    {
        QMessageBox::critical(this, tr("EROOR"),tr("Item not found"));
        return;
    }
    DialogEdit *dialog=new DialogEdit();
    QLineEdit *addNameCity = dialog->findChild<QLineEdit*>("NameCityEdit");
    QLineEdit *addPresedentName = dialog->findChild<QLineEdit*>("PresedentNameEdit");
    QLineEdit *addAverage = dialog->findChild<QLineEdit*>("averageEdit");
    QLabel * label_City_Name = this->findChild<QLabel*>("label_City_Name");
    QLabel * label_Precident_Name = this->findChild<QLabel*>("label_Precident_Name");
    QLabel * label_City_Average = this->findChild<QLabel*>("label_City_Average");
    addNameCity->setText(label_City_Name->text());
    addPresedentName->setText(label_Precident_Name->text());
    addAverage->setText(label_City_Average->text());
    dialog->show();
    int result = dialog->exec();
    if (result == 1)
    {
        if (addNameCity->text().toStdString()!="")
        {
            QString CityName = addNameCity->text();
            QString presidentName = addPresedentName->text();
            int average=addAverage->text().toInt();
            if (average < 1)
            {
                QMessageBox::critical(this, tr("EROOR"),tr("Enter an integer in a string 'average'"));
                return;
            }
            City city(CityName, presidentName, average);
            QVariant qVariant;
            qVariant.setValue(city);
            ui->listWidget->currentItem()->setText(city.getCityName());
            ui->listWidget->currentItem()->setData(Qt::UserRole, qVariant);
            QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
            QLabel * label_City_Name = this->findChild<QLabel*>("label_City_Name");
            QLabel * label_Precident_Name = this->findChild<QLabel*>("label_Precident_Name");
            QLabel * label_City_Average = this->findChild<QLabel*>("label_City_Average");
            label_City_Name->setText(city.getCityName());
            label_Precident_Name->setText(city.getPresidentName());
            label_City_Average->setText(QString::number(city.getAverage()));
        }
    }
}

void MainWindow::on_pushButtonExecute_clicked()
{
    ui->listWidget_result->clear();
    int Execute = ui->lineEdit_Execute->text().toInt();
    if (Execute < 1)
    {
        QMessageBox::critical(this, tr("EROOR"),tr("Enter an integer in a string"));
        return;
    }
    int assert = 1;
    for (int i = 0; i < ui->listWidget->count(); i++)
    {
        QListWidgetItem * selectedItem = ui->listWidget->item(i);
        City city = selectedItem->data(Qt::UserRole).value<City>();
        if (city.getAverage() == Execute)
        {
            QVariant qVariant;
            qVariant.setValue(city);
            QListWidgetItem *qCityListItem = new QListWidgetItem();
            qCityListItem->setText(city.getCityName());
            qCityListItem->setData(Qt::UserRole, qVariant);
            ui->listWidget_result->addItem(qCityListItem);
            assert = 0;
        }
    }
    if (assert == 1)
    {
        QMessageBox::warning(this, tr("EROOR"),tr("City ​​with such an area not found"));
    }
}

void MainWindow::on_listWidget_clicked(const QModelIndex &index)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QLabel * label_City_Name = this->findChild<QLabel*>("label_City_Name");
    QLabel * label_Precident_Name = this->findChild<QLabel*>("label_Precident_Name");
    QLabel * label_City_Average = this->findChild<QLabel*>("label_City_Average");
    QListWidgetItem * selectedItem = items[index.column()];
    City city = selectedItem->data(Qt::UserRole).value<City>();
    label_City_Name->setText(city.getCityName());
    label_Precident_Name->setText(city.getPresidentName());
    label_City_Average->setText(QString::number(city.getAverage()));
}

void MainWindow::on_listWidget_result_clicked(const QModelIndex &index)
{
    QList<QListWidgetItem*> items = ui->listWidget_result->selectedItems();
    QLabel * label_City_Name = this->findChild<QLabel*>("label_City_Name");
    QLabel * label_Precident_Name = this->findChild<QLabel*>("label_Precident_Name");
    QLabel * label_City_Average = this->findChild<QLabel*>("label_City_Average");
    QListWidgetItem * selectedItem = items[index.column()];
    City city = selectedItem->data(Qt::UserRole).value<City>();
    label_City_Name->setText(city.getCityName());
    label_Precident_Name->setText(city.getPresidentName());
    label_City_Average->setText(QString::number(city.getAverage()));
}

void MainWindow::on_actionSave_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save City List..."), "",tr("JSON (*.json)"));
    if (fileName != "")
    {
        QJsonObject jobj;
        QJsonDocument qjdoc;
        QJsonArray citysArr;
        for (int i = 0; i < ui->listWidget->count(); i++)
        {
            QJsonObject so;
            QListWidgetItem * selectedItem = ui->listWidget->item(i);
            City city = selectedItem->data(Qt::UserRole).value<City>();
            so.insert("City name", QJsonValue::fromVariant(city.getCityName()));
            so.insert("President name", QJsonValue::fromVariant(city.getPresidentName()));
            so.insert("City average", QJsonValue::fromVariant(city.getAverage()));
            citysArr.append(so);

        }
        jobj.insert("Citys", citysArr);
        qjdoc.setObject(jobj);
        if (fileName.lastIndexOf(".json") != fileName.length() - 5)
        {
            fileName=fileName + ".json";
        }
        QFile jsonFile(fileName);
        jsonFile.open(QFile::WriteOnly);
        jsonFile.write(qjdoc.toJson());
        jsonFile.close();
    }
}

void MainWindow::on_actionDownload_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open City List..."), "",tr("JSON (*.json);;All Files(*)"));
    if (fileName != "")
    {
        QJsonObject jobj;
        QFile jsonFile(fileName);
        jsonFile.open(QFile::ReadOnly);
        QJsonDocument qjdoc = QJsonDocument().fromJson(jsonFile.readAll());
        jobj = qjdoc.object();
        QJsonArray citysArr = jobj.value("Citys").toArray();
        ui->listWidget->clear();
        for (int i = 0; i < citysArr.size(); i++)
        {
            QJsonValue value = citysArr.at(i);
            QJsonObject cityObj = value.toObject();
            QString CityName = cityObj.value("City name").toString();
            QString presidentName = cityObj.value("President name").toString();
            int average = cityObj.value("City average").toInt();
            City city(CityName, presidentName, average);
            QVariant qVariant;
            qVariant.setValue(city);
            QListWidgetItem *qCityListItem = new QListWidgetItem();
            qCityListItem->setText(city.getCityName());
            qCityListItem->setData(Qt::UserRole, qVariant);
            ui->listWidget->addItem(qCityListItem);
        }
        jsonFile.close();
    }
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open City List..."), "",tr("JSON (*.json);;All Files(*)"));
    if (fileName != "")
    {
        QJsonObject jobj;
        QFile jsonFile(fileName);
        jsonFile.open(QFile::ReadOnly);
        QJsonDocument qjdoc = QJsonDocument().fromJson(jsonFile.readAll());
        jobj = qjdoc.object();
        QJsonArray citysArr = jobj.value("Citys").toArray();
        ui->listWidget->clear();
        for (int i = 0; i < citysArr.size(); i++)
        {
            QJsonValue value = citysArr.at(i);
            QJsonObject cityObj = value.toObject();
            QString CityName = cityObj.value("City name").toString();
            QString presidentName = cityObj.value("President name").toString();
            int average = cityObj.value("City average").toInt();
            City city(CityName, presidentName, average);
            QVariant qVariant;
            qVariant.setValue(city);
            QListWidgetItem *qCityListItem = new QListWidgetItem();
            qCityListItem->setText(city.getCityName());
            qCityListItem->setData(Qt::UserRole, qVariant);
            ui->listWidget->addItem(qCityListItem);
        }
        jsonFile.close();
    }
}

void MainWindow::on_actionSave_2_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save City List..."), "",tr("JSON (*.json)"));
    if (fileName != "")
    {
        QJsonObject jobj;
        QJsonDocument qjdoc;
        QJsonArray citysArr;
        for (int i = 0; i < ui->listWidget->count(); i++)
        {
            QJsonObject so;
            QListWidgetItem * selectedItem = ui->listWidget->item(i);
            City city = selectedItem->data(Qt::UserRole).value<City>();
            so.insert("City name", QJsonValue::fromVariant(city.getCityName()));
            so.insert("President name", QJsonValue::fromVariant(city.getPresidentName()));
            so.insert("City average", QJsonValue::fromVariant(city.getAverage()));
            citysArr.append(so);

        }
        jobj.insert("Citys", citysArr);
        qjdoc.setObject(jobj);
        if (fileName.lastIndexOf(".json") != fileName.length() - 5)
        {
            fileName=fileName + ".json";
        }
        QFile jsonFile(fileName);
        jsonFile.open(QFile::WriteOnly);
        jsonFile.write(qjdoc.toJson());
        jsonFile.close();
    }
}
