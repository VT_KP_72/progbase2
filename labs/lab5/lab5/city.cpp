#include "city.h"
#include <iostream>

City::City() { }

City::City(QString & name, QString & president, int average)
{
    this->name = name;
    this->president = president;
    this->average = average;
}

City::~City() { }

QString City::getCityName()
{
    return this->name;
}

QString City::getPresidentName()
{
    return this->president;
}

int City::getAverage()
{
    return this->average;
}
