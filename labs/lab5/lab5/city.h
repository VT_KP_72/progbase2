#ifndef CITY_H
#define CITY_H
#include <QMetaType>
#include <QString>


class City
{
    QString name = "";
    QString president = "";
    int average = 0;

    public:
        City();
        City(QString & name, QString & president, int average);
        ~City();
        QString getCityName();
        QString getPresidentName();
        int getAverage();
};

Q_DECLARE_METATYPE(City)

#endif // CITY_H
