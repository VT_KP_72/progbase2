#-------------------------------------------------
#
# Project created by QtCreator 2018-05-25T17:51:27
#
#-------------------------------------------------

QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab5
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    city.cpp \
    dialog.cpp \
    dialogedit.cpp

HEADERS  += mainwindow.h \
    city.h \
    dialog.h \
    dialogedit.h

FORMS    += mainwindow.ui \
    dialogedit.ui \
    dialog.ui
