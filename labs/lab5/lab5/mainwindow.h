#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_Add_clicked();

    void on_pushButton_Remove_clicked();

    void on_pushButton_Edit_clicked();

    void on_pushButtonExecute_clicked();

    void on_listWidget_clicked(const QModelIndex &index);

    void on_listWidget_result_clicked(const QModelIndex &index);

    void on_actionSave_triggered();

    void on_actionDownload_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_2_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
