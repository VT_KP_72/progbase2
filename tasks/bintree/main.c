#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>
#include <stdio.h>
#include <bintree.h>
#include <bstree.h>

int main(void) {
	int vals[] = {19, -7, 12, 6, 11, 15, -19};
	BSTree * bst = BSTree_new();
	for (int i = 0; i < sizeof(vals)/sizeof(vals[0]); i++) {
		BSTree_insert(bst, vals[i]);
	}
	BSTree_printTraverse(bst);
	puts("\n");
	BSTree_printFormat(bst);
	puts("");
	BSTree_clear(bst);
	return 0;
}
