#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <progbase/events.h>
#include <progbase/console.h>

enum {
	KeyInputEventTypeId
};

typedef struct __Timer Timer;
struct __Timer {
	int elapsdMillis;
	int timeMillis;
};
typedef struct __TimersArray TimersArray;
struct __TimersArray {
	EventHandler * timers;
};

Timer * Timer_new();
void Timer_free(Timer * self);
void Timer_onEvent(EventHandler * self, Event * event);



TimersArray * TimersArray_new();
void TimersArray_free(TimersArray * self);
void TimersArray_onEvent(EventHandler * self, Event * event);

void KeyInputHandler_update(EventHandler * self, Event * event);
void KeyInputListener_update(EventHandler * self, Event * event);
void Timer_handleEvent(EventHandler * self, Event * event);

int main(void) {
	Console_lockInput();

	EventSystem_init();
 
	EventSystem_addHandler(EventHandler_new(NULL, NULL, KeyInputHandler_update));
	EventSystem_addHandler(EventHandler_new(TimersArray_new(), (DestructorFunction)TimersArray_free, TimersArray_onEvent));

	EventSystem_loop();  

	EventSystem_cleanup();
	Console_unlockInput();
	return 0;
}

void KeyInputHandler_update(EventHandler * self, Event * event) {
	if (conIsKeyDown()) {
        char keyCode = getchar(); 
		if (keyCode == 'q') {
			puts("");
			EventSystem_exit();	
		} else {
            char * keyCodeData = malloc(sizeof(char));
		    *keyCodeData = keyCode;
			EventSystem_emit(Event_new(self, KeyInputEventTypeId, keyCodeData, free));
		}
	}
}


TimersArray * TimersArray_new() {
	TimersArray * self = malloc(sizeof(TimersArray));
	self->timers = NULL;
	return self;
}

void TimersArray_free(TimersArray * self) {
	free(self);
}

void TimersArray_onEvent(EventHandler * self, Event * event) {
	TimersArray * timersArray = self->data;
	switch(event->type) {
		case StartEventTypeId: {
			puts("Press [Enter] to start");
			puts("Press [Space] to stop/start");
			puts("Press [Esc] to end");
			puts("Press [q] to exit");
			break;
		}
		case KeyInputEventTypeId: {
			char * keyData = event->data;
			char keyCode = *keyData;
			EventHandler * timerHandler = timersArray->timers;
			if (keyCode == 10) {
				if (timerHandler == NULL) {
					timerHandler = EventHandler_new(Timer_new(), (DestructorFunction)Timer_free,Timer_onEvent);
					timersArray->timers = timerHandler;
					EventSystem_addHandler(timerHandler);					
				}
			}
			if (keyCode == 27) {
				puts("");
				timersArray->timers = NULL;
				EventSystem_removeHandler(timerHandler);
			}
			if (keyCode == 32) {
				if (timerHandler != NULL) {
					puts("");
					timersArray->timers = NULL;
					EventSystem_removeHandler(timerHandler);;				
				} else {
					timerHandler = EventHandler_new(Timer_new(), (DestructorFunction)Timer_free,Timer_onEvent);
					timersArray->timers = timerHandler;
					EventSystem_addHandler(timerHandler);	
				}
			}
			break;
		}
	}
}
Timer * Timer_new() {
	Timer * self = malloc(sizeof(Timer));
	self->elapsdMillis = 0;
	self->timeMillis = 71;
	return self;
}
void Timer_free(Timer * self) {
	free(self);
}
void Timer_onEvent(EventHandler * self, Event * event) {
	Timer * timer = self->data;
	switch(event->type) {
		case UpdateEventTypeId: {
			double elapsdMillis = *(double *)event->data;
			timer->elapsdMillis += (int)elapsdMillis;
			timer->timeMillis -= 1;
			if (timer->timeMillis % 10 == 0) {
				int counter = timer->timeMillis / 10;
				printf("\nTimer %i", counter); 
			}
			if (timer->timeMillis == 0) {
				printf("\n"); 
				EventSystem_removeHandler(self);
			}
			break;
		}
	}
}