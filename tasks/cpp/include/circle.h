#pragma once
#include <string>
#include <iostream>

using namespace std;

class Circles {
    public:
        void print_circle_info();
        void set_name_circle(string);
        void set_radius_circle(float);
        string get_name_circle();
        float get_area_circle();
        float get_radius_circle();
        void calculate_area_circle();
        
    private:
        string nameCircle;
        float areaCircle;
        float radiusCircle;
};