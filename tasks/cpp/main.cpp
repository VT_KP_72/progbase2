//http://www.dreamincode.net/forums/topic/63358-store-class-objects-in-vector/
#include <iostream>
#include <vector>
#include <limits>
#include "circle.h"

using namespace std;

string scanStringValue(string value);
float scanFloatValue(float value);
int scanIntValue(int value);

int main(void) {
    vector<Circles> sircles;
    float radius;
    string name;
    Circles * circles = new Circles;
    int count = 0;
    int sizeCounter = 0;
    cout << "Puts 1 to print circles;\n";
    cout << "Puts 2 to add circles;\n";
    cout << "Puts 3 to print all circles, areas of such men for S;\n";
    cout << "Puts 4 to end;\n";
    do {
        cout << "\nPuts value ";
        count = scanIntValue(count);
        cout << "\n";
        if (count == 4) {
            break;
        }
        switch (count) {
            case 1: {
                cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
                if (sizeCounter == 0) {
                    cout << "Array is empty (ERROR)\n";
                } else {
                    cout << "You puts 1 to print circles;\n";
                    vector<Circles>::iterator it;
                    for (it = sircles.begin(); it != sircles.end(); ++it) {
                        it->print_circle_info();
                    }  
                }
                cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"; 
                break;
            }
            case 2: {
                if (sizeCounter == 10) {
                    cout << "Maximum number of values (ERROR)\n";
                } else {
                    cout << "You puts 2 to add circles;\n";
                    cout << "==============================";
                    cout << "\nName: ";
                    name = scanStringValue(name);
                    cout << "Radius: ";
                    radius = scanFloatValue(radius);
                    cout << "==============================\n";
                    circles->set_name_circle(name);
                    circles->set_radius_circle(radius);
                    circles->calculate_area_circle();
                    sircles.push_back(*circles);
                    cin.get();
                    sizeCounter++;
                }
                break;
            }
            case 3: {
                cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
                cout << "You puts 3 to print all circles, areas of such men for S;\n";
                cout << "Puts S : ";
                int counter = 0;
                int S = 0;
                S = scanIntValue(S);
                vector<Circles>::iterator it;
                for (it = sircles.begin(); it != sircles.end(); ++it) {
                    if (it->get_area_circle() < S) {
                        it->print_circle_info();  
                        counter++;                      
                    }
                }
                if (counter == 0) {
                    cout << "In an array of numbers not less than S\n";
                }
                cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
                break;            
            }
            default: {
                cout << "\nNepravilni vvod\n" << endl;
            }
        }
    } while (1);
    delete circles;
    return 0;
}

string scanStringValue(string value) {
    cin >> value;
    if (cin.fail()) {
        cin.clear(); // clears error flags
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    return value;
}

float scanFloatValue(float value) {
    cin >> value;
    if (cin.fail()) {
        cin.clear(); // clears error flags
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    return value;
}

int scanIntValue(int value) {
    cin >> value;
    if (cin.fail()) {
        cin.clear(); // clears error flags
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    return value;
}