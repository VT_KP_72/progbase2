#include <string>
#include "circle.h"

#define P 3.14

void Circles::print_circle_info() {
    cout << "==============================" << "\nName : " << Circles::nameCircle << "\nRadius : "
        << Circles::radiusCircle << "\nArea : "
        << Circles::areaCircle << "\n==============================" << endl;
}


void Circles::set_name_circle(string circle_name) {
    Circles::nameCircle = circle_name;
}
void Circles::set_radius_circle(float circle_radius) {
    Circles::radiusCircle = circle_radius;
}
string Circles::get_name_circle() {
    return Circles::nameCircle;
}
float Circles::get_area_circle() {
    return Circles::areaCircle;
}
float Circles::get_radius_circle() {
    return Circles::radiusCircle;
}
void Circles::calculate_area_circle() {
    Circles::areaCircle = Circles::radiusCircle * P * Circles::radiusCircle;
}