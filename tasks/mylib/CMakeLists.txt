cmake_minimum_required(VERSION 3.5.1)

project(mylib)

include_directories(include)
file(GLOB SOURCES "src/*.c")

#Generate the shared (STATIC, SHARED or MODULE) library from the sources
add_library(${PROJECT_NAME} STATIC ${SOURCES})


#------------------------------------------------------------------------------

# install library headers
install(
    DIRECTORY      include/     
    DESTINATION    /usr/local/include
    FILES_MATCHING PATTERN "*.h"
)

#Set the location for library installation -- i.e., /usr/local/lib in this case
# not really necessary in this example. Use "sudo make install" to apply
install(
    TARGETS     ${PROJECT_NAME} 
    DESTINATION /usr/local/lib
)
