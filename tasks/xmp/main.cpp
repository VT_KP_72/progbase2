#include <QDebug>
#include <QtXml>
#include <vector>
#include <iostream>

using namespace std;

class Student {
public:
    QString name;
    int     age;
};

class Group {
public:
    QString name;
    vector<Student> students;
};

string groupToXmlString(const Group & g);
Group xmlStringToGroup(string & str);
void printGroup(const Group & g);

int main(int argc, char *argv[])
{
    Group g;
    g.name = "KP-73";
    g.students.push_back(Student{ "Nastya", 17 });
    g.students.push_back(Student{ "Igor", 18 });

    printGroup(g);

    string str = groupToXmlString(g);
    cout << "--------------" 
           << endl << str << endl
           << "--------------" << endl;

    Group g2 = xmlStringToGroup(str);
    printGroup(g2);

    return 0;
}

void printGroup(const Group & g) {
    cout << g.name.toStdString() << endl << "students: " << endl;
    for (const Student & s : g.students) {
          cout << "  student: " << s.name.toStdString() << " | " << s.age << endl;
    }
}


string groupToXmlString(const Group & g) {
    QDomDocument doc;
    QDomElement groupEl = doc.createElement("group");
    groupEl.setAttribute("name", g.name);

    for (const Student & s: g.students) {
          QDomElement studentEl = doc.createElement("student");
          studentEl.setAttribute("name", s.name);
          studentEl.setAttribute("age", s.age);
          groupEl.appendChild(studentEl);
    }

    doc.appendChild(groupEl);
    return doc.toString().toStdString();
}

Group xmlStringToGroup(string & str) {
    QDomDocument doc;
    if (!doc.setContent(QString::fromStdString(str))) {
          cerr << "error parsing xml!" << endl;
          return Group();
    }
    Group g;
    QDomElement root = doc.documentElement();
    QString nameAttrValue = root.attribute("name");
    g.name = nameAttrValue;

    for (int i = 0; i < root.childNodes().length(); i++) {
          QDomNode studentNode = root.childNodes().at(i);
          QDomElement studentEl = studentNode.toElement();
          Student s;
          s.name = studentEl.attribute("name");
          s.age = studentEl.attribute("age").toInt();
          g.students.push_back(s);
    }

    return g;
}