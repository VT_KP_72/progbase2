#include <stdlib.h>
#include <stdio.h>
#include <numlist.h>


void List_add(NumList * self, int value) {
    LNode * node = NumList_new(value);
    if (self->head == NULL) {
          self->head = node;
          return;
    }
    LNode * cur = self->head;
    while (cur->next != NULL) {
          cur = cur->next;
    }
    cur->next = node;
}

LNode * NumList_new(int value) {
    LNode * node = (LNode *)malloc(sizeof(LNode));
    node->next = NULL;
    node->value = value;
    return node;
}

void removeValuesLessZero(NumList * self) {
    LNode * cur = self->head;
    int i = 0;
    while (cur != NULL) {
        if (cur->value < 0) {
            if (i == 0) {
                LNode * node = self->head;
                self->head = node->next;
                ListNode_free(node);
            } else {
                int j = 0;
                LNode * cur = self->head;
                while (cur->next->next != NULL && j != i - 1) {
                      j += 1;
                      cur = cur->next;
                }
                LNode * node = cur->next;
                int value = node->value;
                cur->next = node->next;
                ListNode_free(node);
            }
            i--;
        }
        cur = cur->next;
        i++;
    }
}

void ListNode_free(LNode * self) {
    free(self);
}
