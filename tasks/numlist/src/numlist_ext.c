#include <stdlib.h>
#include <stdio.h>
#include <numlist_ext.h>

typedef struct __LNode LNode;
struct __NumList {
    LNode * head;
};
struct __LNode {
    int value;
    LNode * next;
};


void List_print(NumList * self) {
    LNode * cur = self->head;
    while (cur != NULL) {
        printf("%i, ", cur->value);
        cur = cur->next;
    }
}