#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <numlist.h>
#include <numlist_ext.h>

int main(void){
    NumList list = {
        .head = NULL
    };
    int a = -100;
    int b = 100;
    puts("=================================================================================");
    srand(time(NULL));
    for (int i = 0; i < 10; i++) {
        int value = rand() % (b - a + 1) + a;
        List_add(&list, value);
    }
    List_print(&list);
    puts("");
    removeValuesLessZero(&list);
    List_print(&list);
	puts("");
    puts("=================================================================================");
    return 0;
}