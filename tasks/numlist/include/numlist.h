typedef struct __NumList NumList;
typedef struct __LNode LNode;

struct __NumList {
    LNode * head;
};
struct __LNode {
    int value;
    LNode * next;
};

LNode * NumList_new(int value);
void List_add(NumList * self, int value);
void removeValuesLessZero(NumList * self);
void ListNode_free(LNode * self);