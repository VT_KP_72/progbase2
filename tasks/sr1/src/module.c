
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#include "../include/module.h"
static const int INITIAL_CAPASITY = 16;

struct __Set {
    int * item;
    int capasity;
    int length;
};
static void incMemory(Set * self){
    self->capasity *= 2;
    self->item = realloc(self->item, self->capasity * sizeof(int));
    
    if(self->item == NULL){
        assert(0);
    }
}
static void checkIndex(Set * self, int index){
    if(index > self->length){
        assert(0 && "Invalid index");
    }
}
Set * Set_new(void) {
    Set * self = malloc(sizeof(Set));
    // if(self == NULL) {
        
    // }
    self->capasity = INITIAL_CAPASITY;
    self->length = 0;
    self->item = malloc(self->capasity * sizeof(int));
    return self;
}
Set * Set_newCopy(Set * set) {
    Set * self = Set_new();

    return self;

}void Set_free(Set * self) {
    free(self);
}

void Set_insert(Set * self, int value) {
    if(self->length == self->capasity){
        incMemory(self);
    }
    self->item[self->length] = value;
    self->length++;
}
void Set_insertSet(Set * self, Set * other) {
    if(self->length == self->capasity){
        incMemory(self);
    }
}
static void removeAt(Set * self, size_t index) {
    checkIndex(self, index);
    for(int i = index + 1; i < self->length; i++){
        self->item[i - 1] = self->item[i];
    }
    self->length--;
    
    if(self->length <= self->capasity / 3 && self->capasity > INITIAL_CAPASITY){
        self->capasity /= 2;
        self->item = realloc(self->item, self->capasity * sizeof(int));
    }
}
void Set_remove(Set * self, int value) {
    for (int i = 0; i < self->length; i++) {
        if (self->item[i] == value) {
            removeAt(self, Set_size(self));
        }
    }
}
void Set_removeSet(Set * self, Set * other) {

}

bool Set_contains(Set * self, int value) {
    for (int i = 0; i < self->length; i++) {
        if (self->item[i] == value) {
            return true;
        }
    }
    return false;
}



bool Set_containsAny(Set * self, int value[], size_t arrayLen) {
    for (int i = 0; i < self->length; i++) {
        for (int j = 0; j < arrayLen; j++) {
            if (self->item[i] == value[j]) {
                return true;
            }
        }
    }
    return false;
}

size_t Set_size(Set * self) {
    return self->length;
}
    

/*
    use qsort() here, ascending order
*/
void Set_copyToArraySorted(Set * self, int array[]) {
    for (int i = 0; i < self->length; i++) {
        array[i] = self->item[i];
    }
}

// set operations
Set * Set_newUnion(Set * self, Set * other) {
    return self;
}

bool Set_equals(Set * self, Set * other) {
    return true;
}
/*
    position independent
*/
bool Set_equalsArray(Set * self, int array[], size_t arrayLen) {
    return true;    
}