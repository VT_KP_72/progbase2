// module.h

#pragma once

#include <stdlib.h>
#include <stdbool.h>

/*
    A set of unique integer values
    Ignore duplicates in operations
*/
typedef struct __Set Set;

Set * Set_new(void);
Set * Set_newCopy(Set * set);
void Set_free(Set * self);

void Set_insert(Set * self, int value);
void Set_insertSet(Set * self, Set * other);

void Set_remove(Set * self, int value);
void Set_removeSet(Set * self, Set * other);

bool Set_contains(Set * self, int value);
bool Set_containsAny(Set * self, int value[], size_t arrayLen);

size_t Set_size(Set * self);
/*
    use qsort() here, ascending order
*/
void Set_copyToArraySorted(Set * self, int array[]);

// set operations
Set * Set_newUnion(Set * self, Set * other);

bool Set_equals(Set * self, Set * other);
/*
    position independent
*/
bool Set_equalsArray(Set * self, int array[], size_t arrayLen);